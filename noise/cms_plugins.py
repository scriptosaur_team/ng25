from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Noise


@plugin_pool.register_plugin
class NoisePlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Noise')
    model = Noise
    render_template = 'noise/noise.html'
    cache = False

    def render(self, context, instance, placeholder):
        request = context['request']
        context['noise_off'] = request.COOKIES.get('noise') == 'off'
        return super().render(context, instance, placeholder)
