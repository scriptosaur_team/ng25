from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from filer.fields.file import FilerFileField


class Noise(CMSPlugin):
    sound = FilerFileField(verbose_name=_('sound'), related_name='noise_sound')

    def __str__(self):
        return self.sound.url
