from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class NoiseConfig(AppConfig):
    name = 'noise'
    verbose_name = _('Noise')
