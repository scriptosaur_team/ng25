from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib.sites.shortcuts import get_current_site


@plugin_pool.register_plugin
class SharingPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Sharing')
    render_template = 'sharing/sharing_plugin.html'

    def render(self, context, instance, placeholder):
        request = context['request']
        try:
            image_obj = request.current_page.heroextension.sharing_image.url \
                        or request.current_page.heroextension.photo.url
            context['photo_url'] = "http://{}{}".format(
                get_current_site(request),
                image_obj.url
            )
        except:
            pass
        return super().render(context, instance, placeholder)
