from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MagicPhotoConfig(AppConfig):
    name = 'magic_photo'
    verbose_name = _('Magic Photo')
