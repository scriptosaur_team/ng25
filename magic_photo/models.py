from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from filer.fields.file import FilerFileField


class MagicPhoto(CMSPlugin):

    STYLE_CHOICES = (
        ('light', _('light')),
        ('dark', _('dark')),
    )

    title = models.CharField(_('title'), max_length=255)
    subtitle = models.CharField(_('subtitle'), max_length=255, blank=True, null=True)
    video = FilerFileField(verbose_name=_('video'), related_name='magic_photo_video', blank=True, null=True)
    region = models.CharField(_('region'), max_length=255, blank=True, null=True)
    style = models.CharField(_('style'), choices=STYLE_CHOICES, default='light', max_length=16)

    def __str__(self):
        return self.title
