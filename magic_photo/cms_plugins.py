from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import MagicPhoto
from hero_page_extension.models import HeroExtension
from django.db.models import Q


@plugin_pool.register_plugin
class MagicPhotoPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Magic Photo')
    model = MagicPhoto
    render_template = 'magic_photo/magic_photo.html'

    def render(self, context, instance, placeholder):
        request = context['request']
        current_page = request.current_page
        queryset = HeroExtension.objects.all() \
                           .exclude(Q(hero_name__isnull=True) | Q(hero_name="")) \
                           .filter(extended_object__publisher_is_draft=False) \
                           .exclude(extended_object__id=request.current_page.id) \
                           .order_by('extended_object__id')

        page_prev = queryset.filter(extended_object__id__lt=current_page.id).last()
        page_next = queryset.filter(extended_object__id__gt=current_page.id).first()

        context.update({
            'page_prev': page_prev,
            'page_next': page_next,
        })

        return super().render(context, instance, placeholder)
