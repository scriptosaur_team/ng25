from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class QuoteConfig(AppConfig):
    name = 'quote'
    verbose_name = _('Quote')
