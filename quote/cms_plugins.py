from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Quote


@plugin_pool.register_plugin
class QuotePlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Quote')
    model = Quote
    render_template = 'quote/quote.html'
