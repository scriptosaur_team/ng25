from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin


class Quote(CMSPlugin):
    quote = models.TextField(_('quote'), help_text=_('HTML tags allowed'))
    borders = models.BooleanField(_('borders'), default=False)

    def __str__(self):
        return self.quote
