import gulp from 'gulp'
import gutil from 'gulp-util'
import clean from 'gulp-clean'

import uglify from 'gulp-uglify'
import named from 'vinyl-named'
import webpack from 'webpack'
import webpackStream from 'webpack-stream'

import sass from 'gulp-sass'
import sourcemaps from 'gulp-sourcemaps'
import postcss from 'gulp-postcss'
import autoprefixer from 'autoprefixer'
import assets from 'postcss-assets'
import cssnano from 'cssnano'


const
    dirs = {
        npm: './node_modules',
        src: './frontend',
        data_images: './frontend/data-images',
        src_images: './frontend/images',
        dest: './main/static',
        vendor: './frontend/vendor',
    },
    files = {
        source: {
            scripts: [
                `${dirs.src}/scripts/app.js`,
            ],
            styles: [
                `${dirs.src}/styles/styles.sass`
            ]
        },
        dest: {
            scripts: `${dirs.dest}/scripts`,
            img: `${dirs.dest}/img`,
            styles: `${dirs.dest}/styles`,
            vendor: `${dirs.dest}/vendor`,
        }
    },
    production = gutil.env.type === 'production';


gulp.task('clean', () => {
    for(let i of Object.keys(files.dest)){
        gulp.src(files.dest[i], {read: false}).pipe(clean());
    }
});

gulp.task('copy', () => {
    gulp.src(`${dirs.vendor}/*.*`)
        .pipe(gulp.dest(files.dest.vendor));
});



gulp.task('css', () => {
    let processors = [
        autoprefixer,
        assets({loadPaths: ['frontend/data-images/']}),
    ];
    if(production) {
        processors.push(cssnano);
    }
    return gulp.src(files.source.styles)
        .pipe(production ? gutil.noop() : sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(production ? gutil.noop() : sourcemaps.write())
        .pipe(gulp.dest(files.dest.styles));
});


gulp.task('js', () => {
    return gulp.src(files.source.scripts)
        .pipe(named())
        .pipe(webpackStream({
            watch: !production,
            devtool: production ? null : 'source-map',
            resolve: {
                extensions: ['', '.js', '.jsx'],
                alias: {
                    "jquery-ui": "jquery-ui/jquery-ui.js",
                    modules: dirs.npm,
                }
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loader: ['babel-loader'],
                        exclude: /node_modules/,
                        query: {
                            plugins: ['transform-runtime', 'transform-decorators-legacy'],
                            presets: ['es2015', 'stage-0']
                        }
                    }
                ]
            },
            plugins:[
                new webpack.DefinePlugin({
                    'process.env':{
                        'NODE_ENV': production ? JSON.stringify('production') : process.env.NODE_ENV
                    }
                }),
            ]
        }))
        .pipe(production ? uglify() : gutil.noop())
        .pipe(gulp.dest(files.dest.scripts));
});


gulp.task('watch', () => {
    gulp.watch(`${dirs.src}/**/*.{sass,scss,svg}`, ['css']);
});


let task_pool = ['copy', 'js', 'css'];
if(gutil.env.type !== 'production') {
    task_pool.push('watch');
}


gulp.task('default', task_pool);
