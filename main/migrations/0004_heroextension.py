# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-15 11:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('main', '0003_simplevideo'),
    ]

    operations = [
        migrations.CreateModel(
            name='HeroExtension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=320, verbose_name="hero's name")),
                ('place', models.CharField(max_length=320, verbose_name='place')),
                ('photographer', models.CharField(max_length=320, verbose_name='photographer')),
                ('extended_object', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='cms.Page')),
                ('photo', filer.fields.file.FilerFileField(on_delete=django.db.models.deletion.CASCADE, to='filer.File', verbose_name='photo')),
                ('public_extension', models.OneToOneField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='draft_extension', to='main.HeroExtension')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
