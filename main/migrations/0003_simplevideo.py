# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-15 08:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('main', '0002_auto_20161210_1855'),
    ]

    operations = [
        migrations.CreateModel(
            name='SimpleVideo',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='main_simplevideo', serialize=False, to='cms.CMSPlugin')),
                ('video', filer.fields.file.FilerFileField(on_delete=django.db.models.deletion.CASCADE, to='filer.File', verbose_name='simple video')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
