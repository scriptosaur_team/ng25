from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from filer.fields.file import FilerFileField

from cms.extensions import PageExtension


class Style(CMSPlugin):

    STYLE_CHOICES = (
        ('default', _('default text')),
        ('paper', _('text on paper')),
        ('large_font', _('large font')),
    )

    style = models.CharField(_('style'), choices=STYLE_CHOICES,  max_length=160)

    def __str__(self):
        return self.style


class SimpleVideo(CMSPlugin):
    video = FilerFileField(verbose_name=_('simple video'))
    wide = models.BooleanField(_('screen wide'), default=False)

    def __str__(self):
        return self.video.url


class Logo(CMSPlugin):
    css_class = models.CharField(_('css class'), max_length=64, blank=True, null=True)

    def __str__(self):
        return self.css_class or str(self.pk)
