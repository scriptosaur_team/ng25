from django import template
from django.contrib.sites.shortcuts import get_current_site

register = template.Library()


@register.inclusion_tag("share/share_meta_tags.html", takes_context=True)
def share_meta_tags(context):
    share_data = {}
    try:
        request = context['request']
        ex = request.current_page.heroextension
        photo = ex.sharing_image or ex.photo
        if photo:
            photo_url = "http://{}{}".format(
                get_current_site(request),
                photo.url
            )
            if photo_url:
                share_data['image'] = photo_url
        if ex.name:
            share_data['title'] = ex.name
        if ex.description:
            share_data['description'] = ex.description
    except:
        pass
    return share_data
