from django import template

register = template.Library()


@register.inclusion_tag("site_graphics/logo.html")
def logo(css_class=None):
    return {
        "css_class": css_class
    }


@register.inclusion_tag("site_graphics/camera.svg")
def camera(css_class=None):
    return {
        "css_class": css_class
    }


@register.inclusion_tag("site_graphics/map_logo.svg")
def map_logo(css_class=None):
    return {
        "css_class": css_class
    }
