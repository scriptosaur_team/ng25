from django import template

register = template.Library()


@register.inclusion_tag("video_icons/loading.svg")
def loading(css_class=None):
    return {
        "css_class": css_class
    }


@register.inclusion_tag("video_icons/play.svg")
def play(css_class=None):
    return {
        "css_class": css_class
    }


@register.inclusion_tag("video_icons/pause.svg")
def pause(css_class=None):
    return {
        "css_class": css_class
    }
