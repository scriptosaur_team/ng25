from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Style, SimpleVideo, Logo


@plugin_pool.register_plugin
class StylePlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Style')
    model = Style
    render_template = 'main/style.html'
    allow_children = True


@plugin_pool.register_plugin
class HrPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Hr')
    render_template = 'main/hr.html'


@plugin_pool.register_plugin
class SimpleVideoPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Simple Video')
    model = SimpleVideo
    render_template = 'main/simple_video.html'


@plugin_pool.register_plugin
class LogoPlugin(CMSPluginBase):
    module = _('Main')
    name = _('Logo')
    model = Logo
    render_template = 'main/logo.html'
