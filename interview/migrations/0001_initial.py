# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-11 10:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterviewVideo',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='interview_interviewvideo', serialize=False, to='cms.CMSPlugin')),
                ('video', filer.fields.file.FilerFileField(on_delete=django.db.models.deletion.CASCADE, related_name='interview_video', to='filer.File', verbose_name='video')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
