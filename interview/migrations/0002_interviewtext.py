# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-11 16:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterviewText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=320, verbose_name='question')),
                ('answer', models.CharField(max_length=320, verbose_name='answer')),
                ('time', models.CharField(max_length=10, verbose_name='time')),
                ('interview', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='interview.InterviewVideo')),
            ],
        ),
    ]
