from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InterviewConfig(AppConfig):
    name = 'interview'
    verbose_name = _('Interview')
