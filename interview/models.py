from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from filer.fields.file import FilerFileField


class Interview(CMSPlugin):
    video = FilerFileField(verbose_name=_('video'), blank=True, null=True)

    def copy_relations(self, oldinstance):
        self.questions.all().delete()
        for question in oldinstance.questions.all():
            question.pk = None
            question.interview = self
            question.save()

    def __str__(self):
        return self.video.url if self.video else str(self.id)


class Question(models.Model):
    interview = models.ForeignKey(Interview, related_name="questions")
    question = models.TextField(_('question'))
    answer = models.TextField(_('answer'))
    time = models.PositiveIntegerField(_('time'), blank=True, null=True)
    ordering = models.IntegerField(_('ordering'), default=1)

    def __str__(self):
        return self.question

    class Meta:
        ordering = ['ordering', 'time']
        verbose_name = _('question')
        verbose_name_plural = _('questions')
