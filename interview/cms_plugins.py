from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Interview, Question


class InterviewLinkInline(admin.TabularInline):
    model = Question


@plugin_pool.register_plugin
class InterviewPlugin(CMSPluginBase):
    module = _('Stories')
    model = Interview
    name = _('Interview')
    render_template = 'interview/interview_plugin.html'
    inlines = [InterviewLinkInline]

    def render(self, context, instance, placeholder):
        context['text'] = instance.questions.all()
        context['questions'] = instance.questions.filter(time__isnull=False).order_by('time')
        return super().render(context, instance, placeholder)
