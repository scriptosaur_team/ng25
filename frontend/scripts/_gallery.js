const $galleries = $(".royalSlider");
const $slides = $(".js-gallery-slide");


$galleries.each(function(){
    const $gallery = $(this);
    let hT;
    $gallery
        .royalSlider({
            fullscreen: {
                enabled: true,
                nativeFS: true
            },
            arrowsNav: true,
            arrowsNavAutoHide: false,
            loop: true,
            keyboardNavEnabled: true,
        })
        .on({
            "mouseover mousemove": () => {
                $gallery.addClass("show-navs");
                clearTimeout(hT);
                hT = setTimeout(() => $gallery.removeClass("show-navs"), 2000);
            },
            "mouseout": function(){
                $gallery.removeClass("show-navs");
            }
        });
});

$slides.each(function(){
    const $button = $(this).find(".js-gallery-more-info-button");
    $button.on("click", function(e){
        e.stopPropagation();
        $galleries.toggleClass("gallery-show-info");
    });
});
