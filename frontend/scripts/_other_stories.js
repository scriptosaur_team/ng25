import Cookies from 'js-cookie'


const pageVisitRemember = 2; // days


$(function () {
    const pageId = parseInt(window.pageId);

    let pagesVisited = Cookies.getJSON("pagesVisited");
    if(!Array.isArray(pagesVisited)) {
        pagesVisited = [];
    }
    pagesVisited = pagesVisited.filter((id, i) => Number.isInteger(id) && pagesVisited.indexOf(id) == i);

    if(!pagesVisited.includes(pageId)){
        pagesVisited.push(pageId);
    }
    Cookies.set("pagesVisited", pagesVisited, {expires: pageVisitRemember});
});
