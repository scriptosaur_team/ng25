import Parallax from 'parallax-scroll'


const parallax = new Parallax('.js-parallax', {
    speed: 0.3,
});

parallax.animate();
