import * as noise from './_noise'


$(function(){

    const $sVideo = $(".js-simple-video");
    const $interview = $(".js-interview-video");

    $sVideo.each(function(){
        let $v = $(this),
            $video = $v.find("video"),
            videoElement = $video[0],
            hT;


        function play() {
            $interview.each(function(){
                $(this).removeClass("playing show-pause").addClass("ready");
                $(this).find("video")[0].pause();
            });
            videoElement.play();
        }

        $video.on('canplaythrough', () => {
            $v
                .removeClass("playing")
                .addClass("ready");
        });

        $v.on({
            "click": () => {
                if(videoElement.paused) {
                    // videoElement.play();
                    play();
                    noise.noiseFadeOut(false);
                    $v
                        .removeClass("ready show-pause")
                        .addClass("playing just-started");
                    setTimeout(() => $v.removeClass("just-started"), 2000);
                } else {
                    videoElement.pause();
                    noise.noiseRestore();
                    $v
                        .removeClass("playing show-pause")
                        .addClass("ready");
                }
            },
            "mouseover mousemove": function(){
                if($v.hasClass("playing")) {
                    $v.addClass("show-pause");
                    clearTimeout(hT);
                    hT = setTimeout(() => $v.removeClass("show-pause"), 2000);
                }
            },
            "mouseout": function(){
                $v.removeClass("show-pause");
            }
        })
    });
});
