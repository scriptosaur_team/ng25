import Cookies from 'js-cookie'


const $noise = $(".noise");
const noiseCookieLifeTime = 2; // days


export function noiseFadeIn(remember=true) {
    $noise.removeClass("muted").each(function(){
        const player = $(this).find("audio")[0];
        let volume = 0;
        player.volume = volume;
        player.play();
        let volumeInc = () => {
            volume += 0.01;
            if(volume < 1) {
                player.volume = volume;
                setTimeout(volumeInc, 20);
            }
        };
        volumeInc();
    });
    if(remember) {
        Cookies.set('noise', 'on', {expires: noiseCookieLifeTime});
    }
}

export function noiseFadeOut(remember=true) {
    $noise.addClass("muted").each(function(){
        const player = $(this).find("audio")[0];
        let volume = player.volume;
        let volumeDec = () => {
            volume -= 0.01;
            if(volume > 0) {
                player.volume = volume;
                setTimeout(volumeDec, 20);
            } else {
                player.pause();
            }
        };
        volumeDec();
    });
    if(remember) {
        Cookies.set('noise', 'off', {expires: noiseCookieLifeTime});
    }
}

export function noiseRestore(){
    const muted = Cookies.get('noise') == 'off';
    if(!muted) {
        noiseFadeIn(false);
    }
}


$(function(){
    if($noise.length > 0) {
        $noise.on("click", function () {
            if ($(this).hasClass("muted")) {
                noiseFadeIn();
            } else {
                noiseFadeOut();
            }
        });
        if (!$noise.hasClass("muted")) {
            noiseFadeIn();
        }
    }
});
