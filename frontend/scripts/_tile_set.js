define([ "jquery", "jquery.panzoom" ], function( $ ) {
    $(function(){
        const $panzoom = $(".js-panzoom");
        const $parent = $panzoom.parent();
        const $mainTile = $(".tile--mainPage");

        let centerX, centerY;

        if($mainTile.length <= 0) {
            centerX = (parseInt($parent.width()) - parseInt($panzoom.width())) / 2;
            centerY = (parseInt($parent.height()) - parseInt($panzoom.height())) / 2
        } else {
            centerX = (parseInt($parent.width()) / 2 - (parseInt($mainTile.css("left")) + $mainTile.width() / 2) - 80);
            centerY = (parseInt($parent.height()) / 2 - (parseInt($mainTile.css("top")) + $mainTile.height() / 2));
        }

        $panzoom.panzoom({
            $zoomIn: $(".js-zoom-in"),
            $zoomOut: $(".js-zoom-out"),
            $zoomRange: $(".js-zoom-range"),
            minScale: .2,
            maxScale: 1,
            increment: 0.1,
        });

        if($mainTile.width() > $(window).width() || $mainTile.height() > $(window).height()) {
            $panzoom.panzoom("zoom", .4);
        }
        $panzoom.panzoom("pan", centerX, centerY);

        $parent.addClass("visible");
        $parent.on('mousewheel.focal', function( e ) {
            e.preventDefault();
            var delta = e.delta || e.originalEvent.wheelDelta;
            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
            $panzoom.panzoom('zoom', zoomOut, {
                increment: 0.1,
                animate: false,
                focal: e
            });
        });
        $panzoom.on('panzoomend', function(e, panzoom, matrix, changed) {
            if (changed) {
                let $link;
                if(e.target.nodeName == "A") {
                    $link = $(e.target);
                } else {
                    $link = $(e.target).closest("a");
                }
                if($link) {
                    $link.one("click", e => e.preventDefault());
                }
            }
        });
        $panzoom.find("a").on('touchstart', e => {
            let $link;
            if(e.target.nodeName == "A") {
                $link = $(e.target);
            } else {
                $link = $(e.target).closest("a");
            }
            $link.one("touchend", e => window.location = $link.attr("href"));
            $panzoom.on('panzoomchange', () => $link.off("touchend"));
        });

    });
});
