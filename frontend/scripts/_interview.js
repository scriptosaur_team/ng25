import * as noise from './_noise'


$(function () {

    const $interview = $(".interview");
    const $sVideo = $(".js-simple-video");

    $interview.each(function(){
        const $v = $(this).find(".js-interview-video");
        const $video = $v.find("video");
        const videoElement = $video[0];
        const $progressBar = $(this).find(".js-interview-progress");
        const $typeSwitcher = $(this).find(".js-interview-switch");
        const $typeSwitchButton = $(this).find(".js-interview-switch-button");
        const $typeVideo = $(this).find(".js-interview-type-video");
        const $typeText = $(this).find(".js-interview-type-text");
        const $questions = $(this).find(".js-interview-question");
        let hT;


        function play() {
            $sVideo.each(function(){
                $(this).removeClass("playing show-pause").addClass("ready");
                $(this).find("video")[0].pause();
            });
            videoElement.play();
        }


        $typeSwitchButton.on("click", function(){
            if($typeSwitcher.hasClass("alt")){
                $typeSwitcher.removeClass("alt");
                $typeText.removeClass("active");
                $typeVideo.addClass("active");
            } else {
                $typeSwitcher.addClass("alt");
                $typeText.addClass("active");
                $typeVideo.removeClass("active");
            }
        });


        $video.on({
            "loadedmetadata": function () {
                const video = this;
                const videoDuration = video.duration;
                let timePoints = [];

                $questions.each(function () {
                    timePoints.push(parseInt($(this).data("time")));
                });
                if (timePoints.length > 0) {
                    for (let i = 0; i < timePoints.length; i++) {
                        let duration = i == 0 ? timePoints[i] : timePoints[i] - timePoints[i - 1],
                            percentage = (100 / videoDuration) * duration;
                        if (i == 0) {
                            $questions.eq(i).css({marginLeft: `${percentage}%`});
                            if (percentage == 0) $questions.eq(i).addClass("start");
                        } else {
                            $questions.eq(i - 1).css({width: `${percentage}%`});
                        }
                        if (i == timePoints.length - 1) {
                            percentage = (100 / videoDuration) * (videoDuration - timePoints[i]);
                            $questions.eq(i).css({width: `${percentage}%`});
                        }
                    }
                }
                $questions.on("click", function (e) {
                    e.stopPropagation();

                    $v
                        .removeClass("ready show-pause")
                        .addClass("playing just-started");
                    // video.play();
                    play();
                    video.pause();
                    video.currentTime = parseInt($(this).data("time"));
                    play();
                    // video.play();
                    noise.noiseFadeOut(false);
                });

                video.addEventListener('timeupdate', function () {
                    $progressBar[0].value = (100 / videoDuration) * video.currentTime;
                    let availablePoints = timePoints.filter(t => t < video.currentTime);
                    if (availablePoints.length > 0) {
                        let p = Math.max(...availablePoints);
                        $questions
                            .removeClass("current")
                            .filter(`[data-time=${p}]`)
                            .addClass("current");
                    }
                });
            },
            "canplay": () => {
                if(!$v.hasClass("playing")) {
                    $v.addClass("ready");
                }
            }
        });
        $v.on({
            "click": () => {
                if(videoElement.paused) {
                    // videoElement.play();
                    play();
                    noise.noiseFadeOut(false);
                    $v
                        .removeClass("ready show-pause")
                        .addClass("playing just-started");
                    setTimeout(() => {
                        $v.removeClass("just-started");
                    }, 2000);
                } else {
                    videoElement.pause();
                    noise.noiseRestore();
                    $v
                        .removeClass("playing show-pause")
                        .addClass("ready");
                }
            },
            "mouseover mousemove": function(){
                if($v.hasClass("playing")) {
                    $v.addClass("show-pause");
                    clearTimeout(hT);
                    hT = setTimeout(() => {
                        $v.removeClass("show-pause");
                    }, 2000);
                }
            },
            "mouseout": function(){
                $v.removeClass("show-pause");
            }
        });
    });

});
