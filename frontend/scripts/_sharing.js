$(function(){
    const heroName = $(".js-hero-name").text();
    const heroQuote = $(".js-hero-quote").first().text();
    const heroPhoto = $(".js-sharing-plugin").data("photo");
    try {
        Ya.share2('sharing', {
            content: {
                title: heroName,
                description: heroQuote,
                image: heroPhoto
            },
            theme: {
                services: 'facebook,twitter,vkontakte,odnoklassniki,gplus'
            }
        });
    } catch (e) {

    }
});
