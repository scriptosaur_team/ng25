from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TileConfig(AppConfig):
    name = 'tile'
    verbose_name = _('Tiles')
