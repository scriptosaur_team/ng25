from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import TileSet, Tile
from .forms import TileForm


@plugin_pool.register_plugin
class TileSetPlugin(CMSPluginBase):
    module = _('Main')
    name = _('Tile Set')
    model = TileSet
    render_template = 'tile/tile_set.html'
    allow_children = True


@plugin_pool.register_plugin
class TilePlugin(CMSPluginBase):
    module = _('Main')
    name = _('Tile')
    model = Tile
    form = TileForm
    render_template = 'tile/tile.html'
    allow_children = True

    fieldsets = (
        (None, {
            'fields': ['page']
        }),
        (_('position'), {
            'fields': ['width', 'height', 'left', 'top']
        }),
        (_('specific text content'), {
            'fields': ['title', 'sub_title', 'text'],
            'classes': ['collapse'],
        }),
        (_('specific decoration'), {
            'fields': ['background', 'css_class', 'header_style', 'text_style'],
            'classes': ['collapse'],
        }),
    )

    def render(self, context, instance, placeholder):
        context.update({
            'title': instance.title,
            'sub_title': instance.sub_title,
            'background': instance.background,
        })
        if instance.page:
            try:
                if not context['title']:
                    context['title'] = instance.page.heroextension.name
                if not context['sub_title']:
                    context['sub_title'] = instance.page.heroextension.place
                if not context['background']:
                    context['background'] = instance.page.heroextension.photo
            except:
                pass

        return super().render(context, instance, placeholder)
