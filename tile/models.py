from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin, Page
from filer.fields.image import FilerFileField


class TileSet(CMSPlugin):
    pass


class Tile(CMSPlugin):

    STYLE_CHOICES = (
        ('white', _('white')),
        ('black', _('black')),
    )

    width = models.PositiveIntegerField(_('width'), default=450)
    height = models.PositiveIntegerField(_('height'), default=450)
    left = models.PositiveIntegerField(_('left'), default=0)
    top = models.PositiveIntegerField(_('top'), default=0)
    title = models.TextField(_('title'), blank=True, null=True)
    sub_title = models.TextField(_('subtitle'), blank=True, null=True)
    text = models.TextField(_('text'), blank=True, null=True)
    page = models.ForeignKey(Page, verbose_name=_('page'), blank=True, null=True, on_delete=models.SET_NULL)
    background = FilerFileField(verbose_name=_('background'), related_name='tile_background', blank=True, null=True)
    header_style = models.CharField(_('header color'), choices=STYLE_CHOICES, default='white', max_length=16,
                                    blank=True, null=True)
    text_style = models.CharField(_('text color'), choices=STYLE_CHOICES, default='white', max_length=16,
                                  blank=True, null=True)
    css_class = models.CharField(_('css class'), max_length=64, blank=True, null=True)

    def __str__(self):
        return self.title or self.page.get_title() if self.page else str(self.pk)

    class Meta:
        verbose_name = _('tile')
        verbose_name_plural = _('tiles')
