from django.forms.models import ModelForm
from django.utils.translation import ugettext_lazy as _
from djangocms_link.fields import PageSearchField
from .models import Tile


class TileForm(ModelForm):

    page = PageSearchField(
        label=_('Page'),
        required=False,
    )

    class Meta:
        model = Tile
        fields = '__all__'
