from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from filer.fields.image import FilerImageField


class Gallery(CMSPlugin):

    def copy_relations(self, oldinstance):
        self.image.all().delete()
        for image in oldinstance.image.all():
            image.pk = None
            image.gallery = self
            image.save()


class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery, related_name="image")
    image = FilerImageField(verbose_name=_('image'), related_name='gallery_image')
    text = models.TextField(_('text'), blank=True, null=True)
    text_left = models.IntegerField(_('text position left'), blank=True, null=True)
    text_top = models.IntegerField(_('text position top'), blank=True, null=True)
    text_width = models.IntegerField(_('text width'), blank=True, null=True)
    text_height = models.IntegerField(_('text height'), blank=True, null=True)
    ordering = models.PositiveIntegerField(_('ordering'), default=100)

    def __str__(self):
        return self.image.url

    class Meta:
        ordering = ['ordering']
        verbose_name = _('image')
        verbose_name_plural = _('images')
