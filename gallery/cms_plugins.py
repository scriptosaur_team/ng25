from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Gallery, GalleryImage


class GalleryImageInline(admin.TabularInline):
    model = GalleryImage


@plugin_pool.register_plugin
class GalleryPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Gallery')
    model = Gallery
    render_template = 'gallery/gallery_plugin.html'
    inlines = [GalleryImageInline]

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        images = instance.image.all()
        context.update({
            'images': images,
        })
        return context
