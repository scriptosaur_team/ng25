# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-11 17:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='galleryimage',
            name='gallery',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='image', to='gallery.GalleryPlugin'),
        ),
        migrations.AlterField(
            model_name='galleryimage',
            name='ordering',
            field=models.PositiveIntegerField(default=100, verbose_name='ordering'),
        ),
    ]
