from django.contrib import admin
from .models import GalleryImage, Gallery


class GalleryImageInline(admin.TabularInline):
    model = GalleryImage


@admin.register
class GalleryPlugin(admin.ModelAdmin):
    model = Gallery
