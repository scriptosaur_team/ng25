from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import MaxWidth


@plugin_pool.register_plugin
class MaxWidthPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('MaxWidth')
    model = MaxWidth
    render_template = 'max_width/max_width.html'
    allow_children = True
