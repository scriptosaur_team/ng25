from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin


class MaxWidth(CMSPlugin):
    max_width = models.PositiveIntegerField(_("max width"), blank=True, null=True)

    def __str__(self):
        return str(self.max_width)
