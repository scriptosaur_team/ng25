from django.apps import AppConfig


class MaxWidthConfig(AppConfig):
    name = 'max_width'
