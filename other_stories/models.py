from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin


class OtherStory(CMSPlugin):

    STYLE_CHOICES = (
        ('light', _('light')),
        ('dark', _('dark')),
    )

    style = models.CharField(_('style'), choices=STYLE_CHOICES, default='light', max_length=16)

    def __str__(self):
        return self.style
