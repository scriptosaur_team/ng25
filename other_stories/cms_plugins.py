from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from hero_page_extension.models import HeroExtension
import json
from urllib import parse
from .models import OtherStory


@plugin_pool.register_plugin
class OtherStoriesPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('OtherStoriesPlugin')
    render_template = 'other_stories/other_stories_plugin.html'
    model = OtherStory
    cache = False

    def render(self, context, instance, placeholder):
        request = context['request']
        try:
            def is_positive_integer(n):
                try:
                    if int(n) > 0:
                        return True
                except:
                    pass
                return False

            pages_visited = [
                i for i in
                list(json.loads(parse.unquote(request.COOKIES.get("pagesVisited"))))
                if is_positive_integer(i)
                ]
        except:
            pages_visited = []
        context.update({
            'cards':
                HeroExtension.objects \
                    .filter(extended_object__publisher_is_draft=False) \
                    .exclude(extended_object__id=request.current_page.id) \
                    .exclude(extended_object__id__in=pages_visited) \
                    .exclude(hero_name__isnull=True) \
                    .exclude(hero_name="") \
                    .order_by("?")[:2]
        })
        return super().render(context, instance, placeholder)
