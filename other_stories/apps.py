from django.apps import AppConfig


class OtherStoriesConfig(AppConfig):
    name = 'other_stories'
