from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField


class Photographer(CMSPlugin):
    LEFT = "left"
    RIGHT = "right"
    TEXT_POSITION = (
        (LEFT, _('left')),
        (RIGHT, _('right')),
    )
    WHITE = "white"
    BLACK = "black"
    TEXT_COLOR = (
        (WHITE, _('white')),
        (BLACK, _('black')),
    )

    name = models.CharField(_('name'), max_length=255)
    description = HTMLField(_('description'))
    photo = FilerImageField(verbose_name=_('photo'), related_name='photographer_photo')
    text_position = models.CharField(_('text position'), max_length=5, choices=TEXT_POSITION, default=LEFT)
    text_color = models.CharField(_('text color'), max_length=5, choices=TEXT_COLOR, default=BLACK)

    def __str__(self):
        return self.name


class PhotographerLink(models.Model):
    photographer = models.ForeignKey(Photographer)
    link = models.URLField(_('URL'))

    def __str__(self):
        return self.link
