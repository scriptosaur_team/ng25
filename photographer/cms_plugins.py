from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Photographer, PhotographerLink


class PhotographerLinkInline(admin.TabularInline):
    model = PhotographerLink


@plugin_pool.register_plugin
class PhotographerPlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Photographer')
    model = Photographer
    render_template = 'photographer/photographer.html'
    inlines = [PhotographerLinkInline]
