from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PhotographerConfig(AppConfig):
    name = 'photographer'
    verbose_name = _('Photographer')
