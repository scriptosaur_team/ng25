from django.contrib import admin
from cms.extensions import PageExtensionAdmin
from .models import HeroExtension
from django.utils.translation import ugettext_lazy as _


class HeroExtensionAdmin(PageExtensionAdmin):
    fieldsets = (
        (_('sharing'), {
            'fields': ['sharing_image', 'name', 'description'],
            'classes': ['collapse'],
        }),
        (_('history card'), {
            'fields': ['hero_name', 'place', 'photographer', 'photo'],
            'classes': ['collapse'],
        }),
        (_('short info for navigation'),{
            'fields': ['short_hero_name', 'short_place'],
            'classes': ['collapse'],
        }),

    )
    pass

admin.site.register(HeroExtension, HeroExtensionAdmin)
