from django.db import models
from filer.fields.file import FilerFileField
from django.utils.translation import ugettext_lazy as _

from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool


@extension_pool.register
class HeroExtension(PageExtension):
    name = models.CharField(_('sharing quote'),  max_length=640, blank=True, null=True)
    hero_name = models.CharField(_('hero name'),  max_length=320, blank=True, null=True)
    place = models.CharField(_('subtitle'),  max_length=320, blank=True, null=True)
    photographer = models.CharField(_('photographer'),  max_length=320, blank=True, null=True)
    photo = FilerFileField(verbose_name=_('image'), blank=True, null=True)
    sharing_image = FilerFileField(verbose_name=_('image for sharing'), blank=True, null=True,
                                   related_name='hero_ex_sharing_image')
    description = models.TextField(_('sharing description'), blank=True, null=True)

    short_hero_name = models.CharField(_('short hero name'),  max_length=320, blank=True, null=True)
    short_place = models.CharField(_('short place'),  max_length=320, blank=True, null=True)

    def __str__(self):
        return self.name or str(self.pk)

    class Meta:
        verbose_name = _('page extra fields')
        verbose_name_plural = _('page extra fields')
