from django.apps import AppConfig


class HeroPageExtensionConfig(AppConfig):
    name = 'hero_page_extension'
