from django import template

register = template.Library()


@register.inclusion_tag("place/ussr.html")
def ussr_map(x=None, y=None):
    return {
        'x': x,
        'y': y
    }
