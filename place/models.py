from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from django.db import models


class Place(CMSPlugin):
    image = FilerImageField(verbose_name=_('image'), related_name='place_image')
    map = FilerImageField(verbose_name=_('map'), related_name='place_map')
    description = HTMLField(_('description'))
    left = models.PositiveIntegerField(_('left'), default=0)
    top = models.PositiveIntegerField(_('top'), default=0)
