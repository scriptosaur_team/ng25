from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Place


@plugin_pool.register_plugin
class PlacePlugin(CMSPluginBase):
    module = _('Stories')
    name = _('Place')
    model = Place
    render_template = 'place/place.html'
