from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PlaceConfig(AppConfig):
    name = 'place'
    verbose_name = _('Place')
